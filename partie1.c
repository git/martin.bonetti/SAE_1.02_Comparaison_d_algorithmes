#include "main.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


char* lireVille(FILE* fe){
    char* Ville;
    fscanf("%s\t", Ville);
    return Ville;
}

char* lireDept(FILE* fe){
    char* Dept;
    fscanf("%s\n", Dept);
    return Dept;
}




ListeDept initListe(void){return NULL;}

ListeDept insertEnTete(ListeDept l, char dep[], int nb, char resp[]){
    MaillonDept *x;
    x = (MaillonDept*)malloc(sizeof(MaillonDept));
    if(x==NULL){printf("pb malloc"); exit;}
    strcpy(x->intitule, dep);
    x->nbP = nb;
    x->suiv = l;
    return x;
}

ListeDept insertCroissante(ListeDept l, char dep[], int nb, char resp[]){
    if (l==NULL){return insertEnTete(l, dep, nb, resp);}
    if (strcmp(l->nomRes, resp)<0){
        return insertEnTete(l,dep, nb, resp);
    }
    if (strcmp(l->nomRes, resp)==0){
        l->nbP=nb;
        strcpy(l->intitule, dep);
        return l;
    }
    l->suiv = insertEnTete(l->suiv, dep, nb, resp);
    return l;
}
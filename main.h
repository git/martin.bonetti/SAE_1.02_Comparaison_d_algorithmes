#include <stdio.h>

typedef struct Maillon{
    char intitule[30];
    int nbP;
    char nomRes[30];
    struct Maillon *suiv;
}MaillonDept, *ListeDept;

typedef struct{
    char ville[30];
    ListeDept ldept;
    VilleIUT *tiut[];
}VilleIUT;

char* lireVille(FILE*);
char* lireDept(FILE*);
ListeDept initListe(void);
ListeDept insertEnTete(ListeDept, char*, int, char*);
ListeDept insertCroissante(ListeDept, char*, int, char*);